# Deep Learning projects

By Michaël EL KHARROUBI and Théo PIRKL

This report presents the results of the implemented code of the two miniprojects, among analysis and conclusions extracted from the data.

# Project 1

This project aims at trying out different architectures of Deep Learning (DL), in order to see how they will react under different circumstances. 

We are working with the MNIST dataset, on exactly $1000$ images. However, we are not to extract only the actual number written in each image received : we are to compare two numbers and predict whether the first number is lesser (or equal) than the other one. Therefore, we are not expected to extract ten classes out of one image (not at the very end, at least) : we are expected to extract a single class, out of two images (one class per image).

We have decided to try two different types of architectures :

- A Convolutional Neural Network (CNN), with a fully connected layer at the end[^1]
- A Multi-Layer Perceptron (MLP), with a fully connected layer at the end[^1]

All models presented below are implemented with data and weight randomization. Each model is also tuned with two hyperparameters : the weight sharing (WS) and the auxiliary loss (AL). The auxiliary loss is based on the two elements of every model described below : the ten classes. We can then use the ten classes <u>and</u> the single class to converge to a result faster.

All models are trained on 100 epochs, 20 rounds, and with a batch size of 100.

Note : while many diagrams and graphs have been generated, we cannot possibly put them all into this report. We've included a `p1/results` folder where all graphs are put in a PDF format. We will mention them as much as possible, when the opportunity presents itself.

## CNN

From the handout 8.2, *Networks for image classification*: 

> The standard model for image classification are the LeNet family (LeCun et al., 1989, 1998), and its modern variants such as AlexNet (Krizhevsky et al., 2012) and VGGNet (Simonyan and Zisserman, 2014).
>
> They share a common structure of several convolutional layers seen as a feature extractor, followed by fully connected layers seen as a classifier.

We've implemented a LeNet classifier (LeNet5). Usualy, images from the MNIST set are 28x28. Given the size of our images are 14x14, we had to change the network a bit to make it "fit" the images. This entierly concerns the convolutional network and not the fully connected layers at the end. 

The main structure of this CNN is as follows :

- The data is passed though the LeNet Convolutional Structure;
- It is then passed to the fully connected layers as defined as in LeNet5; at this point, we have 10 classes. This becomes useful for auxiliary loss purposes.
- Finaly, a single layer is given to convert the 10 classes into 1.

### Results

All results (graphs below as well as confusion matrixes) can be found in this folder : `./p1/results/cnn`. 

| ![](./p1/results/cnn/conf_1/accuracies-training.svg) | ![](./p1/results/cnn/conf_1/accuracies-test.svg) |
| ---------------------------------------------------- | ------------------------------------------------ |

The figures above show the accuracy the model as a function of the epoch, for the training and test set. Note that all lines are a mean of the 20 rounds.

The semitransparent values match the standard deviation (STD) of the measure. We can extract the following deductions : 

- LeNet as a standalone model seems to be quite "noisy" (scattered) as its STD is quite big compared to the other methods.
- LeNet with WS seems to be even noisier. The orange "aura" as shown below is big, the biggest in fact. This suggest our LeNet with WS can give a certain amount of uncertainty in the results.
- While our LeNet implementation with WS seem to suggest results can difer a lot, it is not the case for AL. 
- In fact, AL+WS seems to yield the best results, right after WS.

Apart the STDs, we can also take a look at the data lines themselves : 

- While the accuracy on the training set seem excellent, it is not the case for the test set. In fact, after 20 epochs, we can see that the performance is getting worse with the LeNet WS line. This may be a symptom of overfitting. Let's take a look at the losses.

  | ![](./p1/results/cnn/conf_1/losses-training.svg) | ![](./p1/results/cnn/conf_1/losses-test.svg) |
  | ------------------------------------------------ | -------------------------------------------- |

  Interesting : the losses from the test set start to stagnate or worsen after around 20 epochs. 

  This seems to confirm our suspicions we had with the accuracy figures : there is overfitting with this instance of the model.

  A possible solution for this would be to add images : instead of 1000 images, perhaps 2000 would help. This is because the model will have to generalize more given there is more data at its disposal.

  The figure below shows the same loss, but with 2000 images.

  | ![](./p1/results/cnn/conf_2/losses-training.svg) | ![](./p1/results/cnn/conf_2/losses-test.svg) |
  | ------------------------------------------------ | -------------------------------------------- |

  We can deduce two things :

  - The model was indeed overfitting, as the stagnation seem to have moved around 50 epochs (for the test set)
  - The WS implementation might be faulty, given its sudden rise in loss after 20 epochs. This cannot be explained by any other means at this time.

Those results might indicate that 1000 images is, in this case, not enough. The results with 2000 images are coherent with the rest of the data at our disposal. 

## MLP

In this case, we chose to implement a very simple MLP with two hidden layers. The structure is our MLP is as follows :

- The data is passed through the MLP sequence (two hidden layers, all fully connected). This gives us 10 possible classes.
- The data is then passed a single layer to convert (as in our CNN implementation) the 10 classes to 1.

[^1]: This is used to go from the 10 classes (10 for 10 digits) to 1 class, which classifies if the first image received is bigger or equal than the other one.

Our model always has exactly 196 inputs (14x14, the size of each image), and 1 output. We will only refer to the two hidden layers from this point on.

Different test cases were proposed for this model. To give us a better understanding of how many neurons are required in each layer, we've decided to go from a completely dumb MLP, and then make it smarter and smarter. This also allows to fine-tune our model to use a reasonable amount of neurons per layer.

### Results

All results (graphs below as well as confusion matrixes) can be found in this folder : `./p1/results/mlp`. Note : the configuration of the layers can be found in each subfolder as well. The dumbest model is named `conf_5` and `conf_1` is the smartest.

#### The dumb one

This MLP is quite simple : two layers, with each a single neuron. Let's take a look at the accuracy and the loss.

| ![](./p1/results/mlp/conf_5/accuracies-training.svg) | ![](./p1/results/mlp/conf_5/accuracies-test.svg) |
| ---------------------------------------------------- | ------------------------------------------------ |

This doesn't exactly come as a surprise : the model performs terribly. Which is normal : there is no room to do anything with barely two neurons handling the logic. We can expect performance around 50%, given our classification is a coin toss. In this case,  we can see a raise of performance from the coin toss of 5% (given this model seem to perform around).

We can take a look at the loss functions as well as the confusion matrixes.

| ![](./p1/results/mlp/conf_5/losses-training.svg)             | ![](./p1/results/mlp/conf_5/losses-test.svg)              |
| ------------------------------------------------------------ | --------------------------------------------------------- |
| ![](./p1/results/mlp/conf_5/confmat-training-MLP2, WS+AL.svg) | ![](./p1/results/mlp/conf_5/confmat-test-MLP2, WS+AL.svg) |
| ![](./p1/results/mlp/conf_5/confmat-training-MLP2.svg)       | ![](./p1/results/mlp/conf_5/confmat-test-MLP2.svg)        |

This is hardly overfitting ! The loss function does not seem to worsen, but it might be explained by the terrible, terrible results this model returns. The confusion matrixes show that about 50% of the values are incorrect...

Again, this seems quite normal as two neurons will most definetly not be able to encompass the whole complexitiy of the problem. Given the results, we will not comment on the differences with WS and AL.

From that statement, let's ramp up our layers so that they have more neurons.

### A gentle start

Instead of 2 neurons, we put here 4 neurons and 2 for our two hidden layers. We won't cover in detail what's happening here, because there is a very strong chance we will see the same results as the previous model (that is, bad results), but there are a few interesting worth mentioning.

| ![](./p1/results/mlp/conf_4/accuracies-training.svg) | ![](./p1/results/mlp/conf_4/accuracies-test.svg) |
| ---------------------------------------------------- | ------------------------------------------------ |

Interesting ! With the help of a few extra neurons, we can actually see progress. While the results are still very scattered (as we can see from the STD), the results seem to climb a few percents after 90 epochs. Let's take a look at the confusion matrixes of MLP2, WS and MLP2 :

| ![](./p1/results/mlp/conf_4/confmat-training-MLP2, WS.svg) | ![](./p1/results/mlp/conf_4/confmat-test-MLP2, WS.svg) |
| ---------------------------------------------------------- | ------------------------------------------------------ |
| ![](./p1/results/mlp/conf_4/confmat-training-MLP2.svg)     | ![](./p1/results/mlp/conf_4/confmat-test-MLP2.svg)     |

It seems WS is also enhancing the quality of the results, but not AL. We are unsure as of why.

The increase seem to be very light (we went from 50% of error to 45%), but we're getting there. We've lost a few percents of error already. 

From there, we won't try every slight increase. We now know it's more than reasonable to increase to a bigger number, to see some actual good results.

#### More neurons 

Here, we put 32 neurons in the first layer and 16 in the second. 

| ![](./p1/results/mlp/conf_3/accuracies-training.svg) | ![](./p1/results/mlp/conf_3/accuracies-test.svg) |
| ---------------------------------------------------- | ------------------------------------------------ |

Now we're getting somewhere. The training and the test accuracies look good : the training accuracy reached an acceptable degree of prediction ($\geq 90$% of accuracy). The test accuracy, while not as high, does not seem to worsen or stagnate in our epoch range. To confirm no overfitting, we can take a look at the loss function.

| ![](./p1/results/mlp/conf_3/losses-training.svg) | ![](./p1/results/mlp/conf_3/losses-test.svg) |
| ------------------------------------------------ | -------------------------------------------- |

We spoke too soon ! There is a very slight beginning rise in the loss function at the last epochs on the test loss plot. 100 epochs seem to be a bit too much in this case.

The STD for each of this models seems quite high. Again, this probably is the current lack of neurons. We are still seeing very different results.

We seem to be getting to acceptable predictions : while there is still a lot of disparity in the different values, we're getting to a prediction between 80 and 100%. While this isn't acceptable yet, we're getting there. Let's add **more** neurons !

### MORE neurons

This time, 64 neurons for the first layer, and 32 for the second. As before, let's look at the accuracy.

| ![](./p1/results/mlp/conf_2/accuracies-training.svg) | ![](./p1/results/mlp/conf_2/accuracies-test.svg) |
| ---------------------------------------------------- | ------------------------------------------------ |

The accuracy in the test set seem to go down, which might suggest an overfit. Let's look at the losses.

| ![](./p1/results/mlp/conf_2/losses-training.svg) | ![](./p1/results/mlp/conf_2/losses-test.svg) |
| ------------------------------------------------ | -------------------------------------------- |

There is definetly an overfit after 60 epochs. However, the results at this point are the best we could hope, at around 80-90% of accuracy.

We also can see the raw MLP2 is performing worse than it's "competitors", in loss and accuracy. This is because the two hyperparameters (see above) are here to actually help training. This is then normal and does not go at the opposite of what were expecting. However, the accuracies figure seems to suggest our MLP is way worse without WS and AL.

We feel like we can still go further. Let's add many more neurons and see if this does an actual difference.

### <u>MORE</u> neurons !

This time, we are using a lot of neurons : 512 for the first hidden layer and 256 for the second. Here's the accuracy :

| ![](./p1/results/mlp/conf_1/accuracies-training.svg) | ![](./p1/results/mlp/conf_1/accuracies-test.svg) |
| ---------------------------------------------------- | ------------------------------------------------ |

We still see the same features :

- Overfitting, this time earlier (at approx. 30 epochs) than the previous dataset;
- A maximum performance of either 80 to 100%, depending of the data set. We can see an increase compared to the previous layer. However, this comes at an important computational cost.

Let's take a look at the loss and the confusion matrixes.

| ![](./p1/results/mlp/conf_1/losses-training.svg) | ![](./p1/results/mlp/conf_1/losses-test.svg) |
| ------------------------------------------------ | -------------------------------------------- |

| ![](./p1/results/mlp/conf_1/confmat-training-MLP2.svg) | ![](./p1/results/mlp/conf_1/confmat-test-MLP2.svg) |
| ------------------------------------------------------ | -------------------------------------------------- |

| ![](./p1/results/mlp/conf_1/confmat-training-MLP2, WS+AL.svg) | ![](./p1/results/mlp/conf_1/confmat-test-MLP2, WS+AL.svg) |
| ------------------------------------------------------------ | --------------------------------------------------------- |

By adding neurons, we seem to be shifting the overfit phenomenon to the left. This gives us the opportunity to see how "noisy" some models can be, in particular MLP2. The STD is, compared for example to WS+AL, extremely high. By taking this into account, with the accuracy graph of this model, it seems quite obvious those two hyperparameters are a powerul ally when it comes to supervised learning.



---

### Overview of the results

We've seen different configurations, both for the MLP and the CNN. From our results, we can determine the following :

- While it was never in doubt, experience show that the more neurons, the better the results (especially if you're starting all the way down). It can also cause overfitting to appear faster.
- WS and AL are precious features that allows to make the results more predictable. That is, the STD is usually lower when WS or AL are enabled (as long as they are implemented properly).
- Additional processing (such as with an CNN) seem to prove its efficiency, as the model gets a slightly higher accuracy percentage.
- We can rank the different models by their enable features :
  - Usually, the "raw" model seems to perform the least better;
  - The raw model with WS seems to come next, though it usually produces values with a high STD
  - AL comes next, with good results
  - AL+WS comes first, with almost systematically the best results.

- No result seem to be counter-intuitive of other results.
  
- In a computational sense, it makes sense to try different configurations to see how our model will react, especially with different parameters and its layer configuration. This not only allows us to understand what happens, but also allows us to make our model fit our expectations. There is no interest ramping layers of 2048 neurons when we only need an 80% prediction accuracy.

## Conclusion for projet 1

We have established two different kind of architectures with different hyperparameters, to test their performance and see how they would hold up in specific conditions. We've analyzed the results to try to understand what was happening "under the hood", and found conclusive and coherent data. We've also assessed the performance increase with AL and WS.

We've "only" played with a last fully-connected layer at the end of each model to determine the final class of each image[^2]. What could have been done is a *majority voting*, as described in the paper [*An Ensemble of Simple Convolutional Neural Network Models for MNIST Digit Recognition*](https://arxiv.org/abs/2008.10400). The principle is quite simple : instead of a single model, we take an uneven number of models, trained separately. Then, we "send" a batch of images. Each model will give its prediction. The prediction that is mostly predicted wins.

Unfortunately, we didn't have the time to implement it. Judging by the great results yielded by the paper, we suspect our fully-connected layer at the end of each model to perform in a sub-optimal way. 

[^2]: That is, the class converter from 10 classes to 1.

# Project 2

## Introduction

In the second part of the project we were asked to implement a small framework which could allow us to build small neural nets. In particular, we had to implement the following tools :

- A fully connected layer module,
- the ReLU and Tanh activations,
- a module called Sequential to build a network by stacking layers
- the MSE Loss function
-  the SGD (Stochastic Gradient Descent) optimizer

Secondly, we had to test the smooth running of a neural net built with our framework.  The problem we have is the following :

<figure>
  <img src="./p2/figs/desmos-graph.svg" width="99%" style="display:inline;" alt="Framework test problem">
  <center>
    <figcaption>Fig. 2.1 - The classification problem, is the point inside or outside the circle</figcaption>
  </center>
</figure>

On *Fig. 2.1*, we have a graphical description of the problem presented. We have a random point $p\in\left[0,1\right]^2$ and we would like to know if $p$ is on the red circle, or outside on the blue zone. The center $C$ of the red circle is $C(0.5,0.5)$, and it has a radius of $r=\sqrt{\frac{1}{2\pi}}$ .

## Framework design

As proposed, we tried to design our mini-framework to be as close as possible, in terms of usability, to pytorch. We used the same module system as pytorch. In fact we have three abstract class that form the basis of our framwork :

- `Module` which represent the concept of a layer (Sequential, Linear, ReLU, Tanh)
- `Loss` which is close to module, cannot be stacked (MSE loss)
- `Optimizer` which encapsulate an optimization  (SGD)

### Module

A module, posses four methods :

- `forward`, this method take a tensor and compute the forward pass. It will also store a reference to the input, if it's needed for the backward pass computations.
- `backward`, this method takes a tensor containing the output gradient and computes the derivative regarding its output. If the modules has some parameters, it will add the corresponding the derivatives.
- `zero_grad`, sets the gradient of the parameters to zero.
- `param`,  returns a reference to the parameters

If we take as example the module `Linear` for fully connected layers. We have the following behavior for each method :

- `forward`, computes $\hat{y} =xW^T+ b$, where $x$ is the input tensor, $W$ the weights of the layer and $b$ the bias. Since we need $x$ to update of $\frac{\partial l}{\partial w}$ , we store it.
- `backward`, we receive the gradient of the next layer as argument with it, we can compute the local gradient with respect to our inputs. We also update the gradient of the weights and the bias.
- `param`, we return an object `Parameter` containing references  to the weights and the bias.
- `zero_grad`, we set $\frac{\partial l}{\partial w} = 0$ and $\frac{\partial l}{\partial b} = 0$

The module also has a specific constructor, in which we can instantiate the different parameters. We followed pytorch for weights and bias initialization we sample $\mathcal{U}(-\sqrt{k},\sqrt{k})$ with $k=\frac{1}{\textit{in features}}$. The module also works with no bias if requested.

### Loss

A Loss, only posses two methods :

- `forward`, which takes a prediction and a target to compute the loss.
- `backward`, which takes nothing and compute the local gradient of the loss. 

For the MSE loss we have :

- `forward`, which computes the MSE and store both inputs for the gradient computation.
- `backward`, which takes nothing and compute the local gradient of MSE computed with respects to the inputs stored during the forward pass. 

To keep some kind of compatibility with pytorch, if asked during the initialization its possible to perform the reduction with a sum instead of averaging, which gives SSE instead of MSE. 

### Optimizer

We implemented the SGD using a class named Optimizer, this class is rather simple in our case. We instiante the object with a list of parameters (that we can retrieve with the param method of a Module) and a learning rate.

The method `step`, perform the gradient descent by subtracting the gradient.

 ## Example of usage 

The simplest example of usage would be something like that :

```python
net: Module = Sequential(
    Linear(10, 100),
    Tanh(),
    Linear(100, 5),
    Tanh()
)
loss: Loss = MSELoss()
optim: Optimizer = SGD(net.param(), eta=0.1)

y_hat = net(x)
l = loss(y_hat, y)
dl_dyhat = loss.backward()
net.backward(dl_dyhat)
optim.step()
```

First from line 1 to 6 we build the net by stacking layers, then on line 7 we instantiate the MSE loss. After on line 8, create an SGD optimizer by passing our net params and the learning rate.

An actual step, is performed on line 10, we first make a forward pass of our net on a input sample, then we compute its loss on the next line. With line 12 and 13 we perform the back propagation and finally on line 14 we perform an optimization step. 

## Test.py

As indicated in the introduction, we had to try to solve the problem presented with a neural network built with our framework.

The suggested architecture was the following :

1. Input layer (2 inputs)
2. Hidden layer (25 inputs)
3. Hidden layer (25 inputs)
4. Hidden layer (25 inputs)
5. Output layer (1 input)

We put the reLU activation after each layer.  With our framework, we can initialize it like this :

```python
net: Module = Sequential(
    Linear(2, 25),  # Input Layer
    ReLU(),
    Linear(25, 25),  # Hidden 1
    ReLU(),
    Linear(25, 25),  # Hidden 2
    ReLU(),
    Linear(25, 25),  # Hidden 3
    ReLU(),
    Linear(25, 1),  # Output layer
    ReLU()
)
```

and then, our SGD optimizer like this :

```python
optim: Optimizer = SGD(net.param(), eta=learning_Rate)
```

Now to train it, we will need to generate a dataset. As indicated, we generate a thousand samples with an uniform distribution. Since the area of the circle is exactly half the area of our domain, the points should be approximatly distributed evenly between our two classes (inside or outside). We generated a thousand samples for training, and another thousand for the testing set.

We performed our training with a 100 epochs, and we used mini-batch SGD, with a mini-batch size of 50. For each epoch, we randomized the training set before splitting it in mini-batches.

The code to train our net with our framework is the following :

```python
for i in range(0, xs.size(0), mini_batch_size):
    mini_batch: Tensor = xs[i:i+mini_batch_size]
    mini_batch_target: Tensor = ts[i:i+mini_batch_size]

    net.zero_grad() # Reset the gradiants for each mini-batch
    for sample, expected in zip(mini_batch, mini_batch_target):
        x2: Tensor = net(sample)
        acc_loss += loss(x2, expected) # Sum the loss
        dl_dx2 = loss.backward() # Recover the first partial gradient for backprop 
        net.backward(dl_dx2) # Perform backprop on the network
	optim.step() # Perform the weight update with mini-batch SGD
```

### Results

As requested, we logged the loss for each epoch and then we computed the number of errors for both the training and testing sets.

For the loss we have :

<figure>
  <img src="./p2/figs/res_test.py.svg" width="99%" style="display:inline;" alt="Loss logging">
  <center>
    <figcaption>Fig. 2.2 - The loss logged during training</figcaption>
  </center>
</figure>

As for the errors we have :

```
train errors:  25/1000
test errors :  39/1000
```

The results indicates that our network is capable of learning, since the loss decrease over the epochs. We thus conclude that our framework is working.

### Issues with the problem presented

Sometimes, we encounter a particular situation where the network cannot the learn. The loss stays constant. After **thoroughly** reviewing our code, without finding the issue, we thought that issue was maybe linked to the model (overflow, maybe reLU isn't the best choice etc...). We also tried to implement another network, with another problem (MNIST), and we didn't encounter the problem. We thus tried to implement the same network using pytorch modules to test if the issue was specific to our implementation. We encountered the exact same issue with pytorch. Since we were out of time, we didn't investigate further. Our main guess is a numerical error during computation with some specific cases.

 ## Conclusion part 2

For this part we built a small framework inspired by pytorch. Then we tested it with a given boolean classification problem. The results obtained seem to indicate that our framework works, and that we can build small networks quite simply.

As a continuation to this small project, we could implement other layers like convolution, pooling or softmax activation. We could also implement  others losses such as cross entropy or KLD.  As for the performances, it would pretty simple to add the support for GPU computation, since we use pytorch tensors. For larger dataset, we could use MPI to perform training on multiple computing units, by averaging gradients before the optimization step. If we had time, we would like to investigate the cause of the issue encountered with the test case.

With this miniproject, we learned a bit more about the insides of pytorch and the building of a neural net. We found that the current solution based on the gradient being a property of the tensor used for computation seems to be more pertinent, and allow a simpler code.

