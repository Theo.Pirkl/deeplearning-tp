from torch import Tensor
import torch
from dataclasses import dataclass


@dataclass
class Parameter():
    param: Tensor
    grad: Tensor
