import torch
from torch import Tensor
from abc import ABC, abstractmethod
from enum import Enum

torch.set_grad_enabled(False)


class Loss(ABC):
    @abstractmethod
    def forward(self, nn_output: Tensor, target: Tensor) -> Tensor:
        raise NotImplementedError

    @abstractmethod
    def backward(self) -> Tensor:
        raise NotImplementedError

    def __call__(self, nn_output: Tensor, target: Tensor) -> Tensor:
        return self.forward(nn_output, target)


class MSELoss(Loss):
    class Reductions(Enum):
        SUM = 1
        MEAN = 2

    __reduction: Reductions
    __nn_output: Tensor
    __target: Tensor

    def __init__(self, reduction: Reductions = Reductions.MEAN):
        super().__init__()
        self.__reduction = reduction

    def forward(self, nn_output: Tensor, target: Tensor) -> Tensor:
        self.__nn_output = nn_output
        self.__target = target
        tmp: Tensor = (target-nn_output).pow(2).sum()
        if self.__reduction == MSELoss.Reductions.MEAN:
            tmp *= 1.0/nn_output.size(0)
        return tmp

    def backward(self) -> Tensor:
        tmp: Tensor = -2 * (self.__target-self.__nn_output)
        if self.__reduction == MSELoss.Reductions.MEAN:
            tmp *= 1.0/self.__nn_output.size(0)
        return tmp
