import torch
from torch import Tensor
from modules import Module, Linear, Tanh, Sequential
from losses import MSELoss, Loss
from optimizers import SGD, Optimizer
from matplotlib import pyplot as plt
from prologue_loader import prologue
from typing import Callable

ZETA = 0.9
ETA = 0.1


def MLP(X_train: Tensor,
        T_train: Tensor,
        show_loss: bool = True,
        mini_batch_size: int = 50,
        nb_epochs: int = 100
        ) -> Callable[[Tensor], Tensor]:
    eta = ETA / mini_batch_size
    neural_net: Module = Sequential(
        Linear(784, 50),
        Tanh(),
        Linear(50, 10),
        Tanh())

    loss: Loss = MSELoss()
    optim: Optimizer = SGD(neural_net.param(), eta=eta)

    losses = []
    for _ in range(nb_epochs):
        # Randomize the train set to randomize the minibatches
        idxs: Tensor = torch.randperm(X_train.size(0))
        xs: Tensor = X_train[idxs].view(X_train.size())
        ts: Tensor = T_train[idxs].view(T_train.size())

        acc_loss: float = 0.0
        for i in range(0, xs.size(0), mini_batch_size):
            mini_batch: Tensor = xs[i:i+mini_batch_size]
            mini_batch_target: Tensor = ts[i:i+mini_batch_size]

            neural_net.zero_grad()
            for sample, expected in zip(mini_batch, mini_batch_target):
                x2: Tensor = neural_net(sample)
                acc_loss += loss(x2, expected)
                dl_dx2 = loss.backward()
                neural_net.backward(dl_dx2)
            optim.step()

        losses.append(acc_loss)

    if show_loss:
        plt.plot(range(len(losses)), losses)
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.show()

    return lambda x: neural_net(x).argmax(0)


X_train, T_train, X_test, T_test = prologue.load_data(
    normalize=True, one_hot_labels=True)
T_train *= ZETA

mlp_trained = MLP(X_train, T_train)
predicted = Tensor([mlp_trained(sample).item() for sample in X_test])
print((predicted-T_test.argmax(1)).count_nonzero().item())
