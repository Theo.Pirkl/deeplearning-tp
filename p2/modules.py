import torch
from torch import Tensor
from typing import Tuple, List
from abc import ABC, abstractmethod
from common import Parameter

torch.set_grad_enabled(False)


class Module(ABC):
    @abstractmethod
    def forward(self, inp: Tensor) -> Tensor:
        raise NotImplementedError

    @abstractmethod
    def backward(self, gradwrtoutput: Tensor) -> Tensor:
        raise NotImplementedError

    def zero_grad(self) -> None:
        pass

    def param(self) -> List[Parameter]:
        return []

    def __call__(self, inp: Tensor) -> Tensor:
        return self.forward(inp)


class Linear(Module):
    weights: Tensor
    bias: Tensor = None
    __inp: Tensor = None
    __dl_w: Tensor = None
    __dl_b: Tensor = None

    def __init__(self, in_feature: int, out_features: int, bias: bool = True):
        super().__init__()
        # Based on pytorch linear initalization
        k: float = 1.0/Tensor([in_feature]).sqrt().item()
        self.weights = torch.empty(out_features, in_feature).uniform_(-k, k)
        self.__dl_w = torch.zeros(out_features, in_feature)
        if bias:
            self.bias = torch.empty(out_features).uniform_(-k, k)
            self.__dl_b = torch.zeros(out_features)

    def forward(self, inp: Tensor) -> Tensor:
        self.__inp = inp
        tmp = (inp @ self.weights.T + self.bias) if self.bias is not None\
            else (inp @ self.weights.T)
        return tmp

    def backward(self, gradwrtoutput: Tensor) -> Tensor:
        dl_dx = self.weights.T @ gradwrtoutput
        self.__dl_w += gradwrtoutput.view(-1, 1) @ self.__inp.view(1, -1)
        if self.bias is not None:
            self.__dl_b += gradwrtoutput
        return dl_dx

    def param(self) -> List[Parameter]:
        res: List[Parameter] = super().param()
        res.append(Parameter(self.weights, self.__dl_w))
        if self.bias is not None:
            res.append(Parameter(self.bias, self.__dl_b))
        return res

    def zero_grad(self) -> None:
        self.__dl_w.zero_()
        if self.bias is not None:
            self.__dl_b.zero_()


class Tanh(Module):
    __inp: Tensor = None

    def __init__(self):
        super().__init__()

    def __derivative(x: Tensor) -> Tensor:
        return 1-x.tanh().pow(2)

    def forward(self, inp: Tensor) -> Tensor:
        self.__inp = inp
        tmp: Tensor = inp.tanh()
        return tmp

    def backward(self, gradwrtoutput: Tensor) -> Tensor:
        dl_dx = gradwrtoutput * Tanh.__derivative(self.__inp)
        return dl_dx


class ReLU(Module):
    __inp: Tensor = None

    def __init__(self):
        super().__init__()

    def __derivative(x: Tensor) -> Tensor:
        # df(x) = 0 if x <= 0, 1 otherwise
        # I choose to set df(0) == 0, to avoid nan
        tmp = torch.max(torch.Tensor([0]), x.sign())
        return tmp

    def forward(self, inp: Tensor) -> Tensor:
        self.__inp = inp
        tmp: Tensor = torch.max(Tensor([0.0]), inp)
        return tmp

    def backward(self, gradwrtoutput: Tensor) -> Tensor:
        dl_dx = gradwrtoutput * ReLU.__derivative(self.__inp)
        return dl_dx


class Sequential(Module):
    layers: List[Module]

    def __init__(self, *layers: Module):
        super().__init__()
        self.layers = []
        for l in layers:
            self.layers.append(l)

    def forward(self, inp: Tensor) -> Tensor:
        tmp: Tensor = inp
        for l in self.layers:
            tmp = l(tmp)
        return tmp

    def backward(self, gradwrtoutput: Tensor) -> Tensor:
        tmp: Tensor = gradwrtoutput
        for l in reversed(self.layers):
            tmp = l.backward(tmp)
        return tmp

    def param(self) -> List[Parameter]:
        res: List[Parameter] = super().param()
        for l in self.layers:
            for p in l.param():
                res.append(p)
        return res

    def zero_grad(self) -> None:
        for l in self.layers:
            l.zero_grad()
