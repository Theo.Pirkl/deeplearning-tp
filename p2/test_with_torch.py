import torch
from torch import Tensor
from torch.nn import Module, Linear, ReLU, Sequential
from torch.nn import MSELoss
from torch.optim import SGD, Optimizer
from matplotlib import pyplot as plt
from typing import Callable, Tuple, List

# This file is just for comparison


def generate_dataset() -> Tuple[Tensor, Tensor, Tensor, Tensor]:
    x_inp: Tensor = torch.empty(2000, 2).uniform_(0, 1)
    x_target: Tensor = 1.0 * \
        (((x_inp[:, 0]-0.5).pow(2) + (x_inp[:, 1]-0.5).pow(2)) <= 1/(2*torch.pi))

    return x_inp[:1000], x_target[:1000], x_inp[1000:], x_target[1000:]


def train_model(x: Tensor,
                target: Tensor,
                net: Module,
                loss: Module,
                optim: Optimizer,
                mini_batch_size: int,
                nb_epochs: int = 100,
                show_loss: bool = True
                ) -> List[float]:
    losses = []
    for _ in range(nb_epochs):
        # Randomize the train set to randomize the minibatches
        idxs: Tensor = torch.randperm(x.size(0))
        xs: Tensor = x[idxs].view(x.size())
        ts: Tensor = target[idxs].view(target.size())

        acc_loss: float = 0.0
        for i in range(0, xs.size(0), mini_batch_size):
            mini_batch: Tensor = xs[i:i+mini_batch_size]
            mini_batch_target: Tensor = ts[i:i+mini_batch_size]

            net.zero_grad()
            for sample, expected in zip(mini_batch, mini_batch_target):
                x2: Tensor = net(sample)
                tmp = loss(x2.view(-1), expected.view(-1))
                tmp.backward()
                acc_loss += tmp

            optim.step()

        losses.append(acc_loss.detach().numpy())

    if show_loss:
        plt.plot(range(len(losses)), losses)
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.show()

    return losses


train_x, train_label, test_x, test_label = generate_dataset()

net: Module = Sequential(
    Linear(2, 25),  # Input Layer
    ReLU(),
    Linear(25, 25),  # Hidden 1
    ReLU(),
    Linear(25, 25),  # Hidden 2
    ReLU(),
    Linear(25, 25),  # Hidden 3
    ReLU(),
    Linear(25, 1),  # Output layer
    ReLU()
)

loss: Module = MSELoss()
mini_batch_size = 50
optim: Optimizer = SGD(net.parameters(), lr=0.1/mini_batch_size)

losses = train_model(train_x, train_label, net, loss, optim, mini_batch_size)

train_preds: Tensor = Tensor([net(x) >= 0.5 for x in train_x])
test_preds: Tensor = Tensor([net(x) >= 0.5 for x in test_x])

print("train errors: ", (train_preds-train_label).count_nonzero().item())
print("test errors : ", (test_preds-test_label).count_nonzero().item())
