import torch
from torch import Tensor
from abc import ABC, abstractmethod
from enum import Enum
from typing import List, Tuple
from common import Parameter

torch.set_grad_enabled(False)


class Optimizer(ABC):
    _params: List[Parameter]

    @abstractmethod
    def __init__(self, parameters: List[Parameter]):
        self._params = parameters

    @abstractmethod
    def step(self) -> Tensor:
        raise NotImplementedError


class SGD(Optimizer):
    __eta: float

    def __init__(self, parameters: List[Parameter], eta: float = 0.1):
        super().__init__(parameters)
        self.__eta = eta

    def step(self) -> Tensor:
        for p in self._params:
            p.param -= self.__eta * p.grad
