import torch
from torch import nn


def narrow(x: torch.Tensor, batch: int, mini_batch_size: int):
    return x.narrow(0, batch, mini_batch_size)


def shuffle(x: torch.Tensor, random_index: torch.Tensor) -> torch.Tensor:
    return x[random_index]


# Taken from
# https://discuss.pytorch.org/t/randomly-set-the-weights-of-part-of-model-layers-for-every-time-training/56639/8
def init_params(m):
    if type(m) == nn.Linear or type(m) == nn.Conv2d:
        m.weight.data = torch.randn(m.weight.size()) * .01  # Random weight initialisation
        m.bias.data = torch.zeros(m.bias.size())
