from typing import Tuple, List

import torch
from matplotlib import pyplot as plt
from torch import nn, optim, FloatTensor
from dlc_practical_prologue import generate_pair_sets
from models.cnn import TwoDigitsNet
from models.mlp import MLP2, Orchestrator
from utils import narrow, shuffle


def train_model(model: nn.Module, train: Tuple[torch.Tensor, torch.Tensor, torch.Tensor],
                test: Tuple[torch.Tensor, torch.Tensor, torch.Tensor],
                epochs: int, mini_batch_size: int) \
        -> Tuple[List[float], List[float], List[float], List[float], List[List[int]], List[List[int]]]:
    """
    Trains the model
    :param model: The model in question
    :param train: Train tuple containing the train_input, train_target and train_classes datasets.
    :param test: Test tuple containing the test_input and test_target datasets.
    :param epochs: The number of epochs required for this model
    :param mini_batch_size: The mini batch size (int)
    """
    criterion, aux = nn.MSELoss(), nn.MSELoss()
    optimizer = optim.SGD(model.parameters(), lr=0.1)

    round_train_loss, round_test_loss = [], []
    round_train_acc, round_test_acc = [], []

    train_input, train_target, target_classes = train[0], train[1], train[2]
    test_input, test_target, test_classes = test[0], test[1], test[2]

    for _ in range(epochs):
        for batch in range(0, train_input.size(0), mini_batch_size):
            input_batch = narrow(train_input, batch, mini_batch_size)
            target_batch = narrow(train_target, batch, mini_batch_size)

            # Actual prediction
            prediction, classes = model(input_batch)

            # Predictions have been made, let's see how bad they are now !
            loss = criterion(prediction, target_batch.float())

            model.zero_grad()

            if classes is not None:
                classes_batch = narrow(target_classes, batch, mini_batch_size)
                loss += aux(classes, classes_batch)

            loss.backward()
            optimizer.step()

        # We now compute the accuracy of the model
        conmat_train = compute_nb_errors(model, train_input, train_target, mini_batch_size)
        conmat_test = compute_nb_errors(model, test_input, test_target, mini_batch_size)

        train_accuracy = len(train_input) - conmat_train[0][1] - conmat_train[1][0]
        test_accuracy = len(test_input) - conmat_test[0][1] - conmat_test[1][0]

        round_train_acc.append((train_accuracy / len(train_input)) * 100)
        round_test_acc.append((test_accuracy / len(test_input)) * 100)

        # Same-same, but for loss
        train_prediction, _ = model(train_input)
        test_prediction, _ = model(test_input)

        round_train_loss.append(criterion(train_prediction, train_target).item())
        round_test_loss.append(criterion(test_prediction, test_target).item())

    return round_train_acc, round_test_acc, round_train_loss, round_test_loss, conmat_train, conmat_test


def compute_nb_errors(model: nn.Module, data: torch.Tensor, labels: torch.Tensor, mini_batch_size: int) \
        -> List[List[int]]:
    matcon = [[0, 0], [0, 0]]
    for batch in range(0, data.size(0), mini_batch_size):
        # Matching size
        output, _ = model(narrow(data, batch, mini_batch_size))
        predicted_classes = [0 if output[i] < 0.5 else 1 for i in range(len(output))]

        for k in range(mini_batch_size):
            actual = labels[batch + k].item()
            practical = predicted_classes[k]
            matcon[actual][practical] += 1

    return matcon


def main():
    nb_pairs, nb_rounds, nb_epochs, batch_size = 1000, 20, 100, 100

    #model_names = ["LeNet", "LeNet, WS", "LeNet, AL", "LeNet, AL+WS"]
    model_names = ["MLP2", "MLP2, WS", "MLP2, AL", "MLP2, WS+AL"]

    train_loss, test_loss = {name: [] for name in model_names}, {name: [] for name in model_names}
    train_accu, test_accu = {name: [] for name in model_names}, {name: [] for name in model_names}
    train_conmat, test_conmat = {name: [] for name in model_names}, {name: [] for name in model_names}

    for round_nb in range(nb_rounds):
        #models = [TwoDigitsNet(False, False), TwoDigitsNet(True, False),
        #         TwoDigitsNet(False, True), TwoDigitsNet(True, True)]
        models = [Orchestrator(MLP2, False, False), Orchestrator(MLP2, True, False),
                  Orchestrator(MLP2, True, False), Orchestrator(MLP2, True, True)]

        instances = {model_names[i]: models[i] for i in range(len(models))}

        print(f"Round {round_nb + 1}")

        rix = torch.randperm(nb_pairs)
        data = generate_pair_sets(nb_pairs)
        train_input, train_target, train_classes = shuffle(data[0], rix), shuffle(data[1], rix), shuffle(data[2], rix)
        test_input, test_target, test_classes = shuffle(data[3], rix), shuffle(data[4], rix), shuffle(data[5], rix)

        # Refinment of data
        train_classes = torch.nn.functional.one_hot(train_classes, num_classes=10).float()
        train = (train_input, train_target, train_classes)
        test = (test_input, test_target, test_classes)

        for i in range(len(instances)):
            name = model_names[i]
            print(f"Now running : {name}")
            instance = instances[name]

            tr_a, te_a, tr_l, te_l, tr_c, te_c = train_model(instance, train, test, nb_epochs, batch_size)

            with torch.no_grad():
                train_accu[name].append(tr_a)
                test_accu[name].append(te_a)
                # Losses
                train_loss[name].append(tr_l)
                test_loss[name].append(te_l)
                # Confusion matrices
                train_conmat[name].append(tr_c)
                test_conmat[name].append(te_c)

    print("Done. Now plotting results.")
    draw((train_accu, test_accu), (train_loss, test_loss), (train_conmat, test_conmat))


def draw(accuracies, losses, conmats) -> None:
    # I am lazy
    quickie = ["training", "test"]
    for cat_name, block in {"accuracies": accuracies, "losses": losses}.items():
        for i, acc_type in enumerate(block):
            fig, ax = plt.subplots(facecolor='w')

            for name, data in acc_type.items():
                x = list(range(len(data[0])))
                data = torch.Tensor(data)
                line = data.mean(dim=0)

                ax.plot(line, label=name)
                ax.fill_between(x, line - data.std(dim=0), line + data.std(dim=0), alpha=.25)

            plt.title(f"Model {quickie[i]} {cat_name} as a function of epoch")
            plt.xlabel("Epoch")
            plt.ylabel("Accuracy" if cat_name == "accuracies" else "Loss")
            plt.legend()
            plt.savefig(f"{cat_name}-{quickie[i]}.pdf")

    # Now drawing conmats
    for cat_name, block in {"training": conmats[0], "test": conmats[1]}.items():
        for name, data in block.items():
            mean = torch.Tensor(data).mean(dim=0)
            std = torch.Tensor(data).std(dim=0)
            fig, ax = plt.subplots(figsize=(7.5, 7.5))

            ax.matshow(mean, cmap=plt.cm.Blues, alpha=0.3)
            for i in range(mean.shape[0]):
                for j in range(mean.shape[1]):
                    ax.text(x=j, y=i, s=f"{mean[i][j].item():.2f}, STD {std[i][j]:.2f}", va='center', ha='center',
                            size='large')

            plt.xlabel('Model predictions', fontsize=18)
            plt.ylabel('MNIST labels', fontsize=18)
            plt.title(f'Confusion Matrix for model {name} ({cat_name})', fontsize=18)
            plt.savefig(f"confmat-{cat_name}-{name}.pdf")


if __name__ == '__main__':
    main()  # This is done to avoid any kind of global vars
