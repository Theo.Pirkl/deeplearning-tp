import torch
from torch import nn


# Implementation of LeNet.
# This will not directly be used. In fact, this is a pure classifier that allows to get the
# predicted number and not much more.
class LeNet(nn.Module):
    def __init__(self):
        super().__init__()

        # Shape of our net
        self.features = nn.Sequential(
            nn.Conv2d(1, 6, 5),  # => 14-5+1 : 6x10x10
            nn.ReLU(),
            nn.AvgPool2d(2, stride=2),  # => 6x5x5
            nn.Conv2d(6, 16, 2, padding=2),  # => 5-2+2*2+1 : 16x8x8
            nn.ReLU(),
            nn.AvgPool2d(2),  # => 16x4x4
        )

        self.classifier = nn.Sequential(
            nn.Linear(256, 120),
            nn.ReLU(),
            nn.Linear(120, 84),
            nn.ReLU(),
            nn.Linear(84, 10),
            nn.Softmax(dim=1),  # One-hot switcheroo
            nn.Flatten()
        )

    def forward(self, data):
        return nn.Sequential(self.features, nn.Flatten(), self.classifier)(data)


# This is the wrapper around the LeNet
class TwoDigitsNet(nn.Module):
    def __init__(self, enable_weight_sharing: bool, enable_aux_loss: bool):
        super().__init__()

        self.enable_weight_sharing = enable_weight_sharing
        self.enable_aux_loss = enable_aux_loss

        self.cpu_1 = LeNet()
        self.cpu_2 = self.cpu_1 if self.enable_weight_sharing else LeNet()

        self.feature_classifier = nn.Sequential(
            nn.Flatten(),
            nn.Linear(20, 1)
        )

    def forward(self, data):
        img1, img2 = data.narrow(1, 0, 1), data.narrow(1, 1, 1)
        lenet_output_1, lenet_output_2 = self.cpu_1(img1), self.cpu_2(img2)
        x = torch.cat((lenet_output_1.unsqueeze(1), lenet_output_2.unsqueeze(1)), 1)

        res = self.feature_classifier(x).flatten()

        return res.float(), x.float() if self.enable_aux_loss else None
