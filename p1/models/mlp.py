import torch
from torch import nn

from p1.utils import init_params


class MLP2(nn.Module):
    def __init__(self):
        super().__init__()

        self.features = nn.Sequential(
            nn.BatchNorm2d(num_features=1),
            nn.Flatten(),
            nn.Linear(196, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 10)
        )

    def forward(self, data):
        return self.features(data)


class Orchestrator(nn.Module):
    def __init__(self, mlp, enable_weight_sharing: bool, enable_aux_loss: bool):
        super().__init__()

        self.enable_weight_sharing = enable_weight_sharing
        self.enable_aux_loss = enable_aux_loss

        self.cpu_1 = mlp()
        self.cpu_2 = self.cpu_1 if self.enable_weight_sharing else mlp()

        # RWI
        self.cpu_1.apply(init_params)
        if not self.enable_weight_sharing:
            self.cpu_2.apply(init_params)

        self.feature_classifier = nn.Sequential(
            nn.Flatten(),
            nn.Linear(20, 1)
        )

    def forward(self, data):
        img1, img2 = data[:, 0:1], data[:, 1:2]
        mlp_output_1, mlp_output_2 = self.cpu_1(img1), self.cpu_2(img2)
        x = torch.cat((mlp_output_1.unsqueeze(1), mlp_output_2.unsqueeze(1)), 1)

        final_result = self.feature_classifier(x).flatten()

        return final_result.float(), x.float() if self.enable_aux_loss else None
